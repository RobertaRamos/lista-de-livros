# __Lista de livros__

## _Objetivo:_ 
O objetivo do aplicativo é aprender a fazer requisições em APIS.

## *Arquitetura usuda:*
A  arquitetura usada será a **MVVM** (Model View ViewModel), recomendada pelo google.

![](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png?hl=pt-br)

## *Dependências usadas:*
- [Retrofit](https://square.github.io/retrofit/) 
