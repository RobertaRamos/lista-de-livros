package com.example.usuarios;

import com.example.usuarios.model.Usuario;
import com.example.usuarios.network.ConfiguraRetrofit;
import com.example.usuarios.network.UsuarioCallback;
import com.example.usuarios.network.UsuariosServices;
import com.example.usuarios.network.RetornoUsuarioAPI;
import com.example.usuarios.repository.UsuariosRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsuarioRepositoryImp implements UsuariosRepository {
    private UsuariosServices service = new ConfiguraRetrofit().getUsuariosServices();


    @Override
    public void getusuarioAPI(UsuarioCallback usuarioCallback) {
        Call<RetornoUsuarioAPI> listaUsuarios = service.getListaUsuarios(20);
        listaUsuarios.enqueue(new Callback<RetornoUsuarioAPI>() {
            @Override
            public void onResponse(Call<RetornoUsuarioAPI> call, Response<RetornoUsuarioAPI> response) {
                if(response.isSuccessful()){
                    List<Usuario> usuarios = response.body().getData();
                    usuarioCallback.sucesso(usuarios);
                }else usuarioCallback.falha(response.message());
            }

            @Override
            public void onFailure(Call<RetornoUsuarioAPI> call, Throwable t) {
                usuarioCallback.falha(t.getMessage());
            }
        });

    }

}
