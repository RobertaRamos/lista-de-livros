package com.example.usuarios.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UsuariosServices {

    @GET("users")//books?_quantity=1
    Call<RetornoUsuarioAPI> getListaUsuarios(@Query("_quantity") int _quantity);
}
