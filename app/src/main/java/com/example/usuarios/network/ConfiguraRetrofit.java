package com.example.usuarios.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConfiguraRetrofit {

    private final String baseURL = "https://fakerapi.it/api/v1/";

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public UsuariosServices getUsuariosServices(){
        return retrofit.create(UsuariosServices.class);
    }



}
