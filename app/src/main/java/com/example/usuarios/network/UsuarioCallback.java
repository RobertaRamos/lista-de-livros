package com.example.usuarios.network;

import com.example.usuarios.model.Usuario;

import java.util.List;

public interface UsuarioCallback {

     void sucesso(List<Usuario> usuarios);
     void falha(String mensagemFalha);
}
