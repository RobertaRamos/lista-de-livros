package com.example.usuarios.network;

import com.example.usuarios.model.Usuario;

import java.util.List;

public class RetornoUsuarioAPI {
    private String status;
    private int code;
    private int total;
    private List<Usuario> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Usuario> getData() {
        return data;
    }

    public void setData(List<Usuario> data) {
        this.data = data;
    }
}
