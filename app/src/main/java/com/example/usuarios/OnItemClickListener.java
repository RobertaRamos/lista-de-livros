package com.example.usuarios;

import com.example.usuarios.model.Usuario;

public interface OnItemClickListener {
    void onItemClick(Usuario usuario);
}
