package com.example.usuarios;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.usuarios.Adapter.ListaUsuariosAdapter;
import com.example.usuarios.model.Usuario;

import java.util.List;

public class ListaUsuariosActivity extends AppCompatActivity {


    public static final String CHAVE_USUARIO = "Usuario clicado";
    private ListaUsuariosViewModel viewModel;
    private View includeTelaErro;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_usuarios);
        forneceViewModel();
        configuraBotaoTentarNovamente();

    }

    private void configuraBotaoTentarNovamente() {
        Button botaoTentarNovamente = findViewById(R.id.erro_aquisicao_lista_botao);
        botaoTentarNovamente.setOnClickListener(v -> buscaLivrosNovamente());
    }

    private void buscaLivrosNovamente() {
        includeTelaErro.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        viewModel.getLivro();
    }


    private void forneceViewModel() {
        viewModel = new ViewModelProvider(this).get(ListaUsuariosViewModel.class);
        observaMetodoViewModel();
    }

    private void observaMetodoViewModel() {
        configuraProgressBar();
        viewModel.getLivro().observe(this, listUsuarioData ->{
            ehDiferenteNulo(listUsuarioData);
            progressBar.setVisibility(View.GONE);
        });
    }

    private void ehDiferenteNulo(ListUsuarioData listUsuarioData) {
        includeTelaErro = configuraInclude();
        if (listUsuarioData.mensagemFalha != null){
            includeTelaErro.setVisibility(View.VISIBLE);


        }else if(listUsuarioData.listUsuarioData != null){
            includeTelaErro.setVisibility(View.GONE);
            List<Usuario> todosUsuraios = listUsuarioData.listUsuarioData;
            configuraRecyclerView(todosUsuraios);
        }
    }

    private View configuraInclude() {
        return findViewById(R.id.tela_erro);
    }

    private void configuraRecyclerView(List<Usuario> todosUsuraios) {
        RecyclerView recyclerView = findViewById(R.id.lista_usuarios_recyclerView);
        configuraAdapter(todosUsuraios, recyclerView);
    }

    private void configuraAdapter(List<Usuario> todosUsuraios, RecyclerView recyclerView) {
        ListaUsuariosAdapter adapter = new ListaUsuariosAdapter(todosUsuraios);
        recyclerView.setAdapter(adapter);
        vaiParaPerfilUsuario(adapter);
    }

    private void vaiParaPerfilUsuario(ListaUsuariosAdapter adapter) {
        adapter.setOnItemClickListener(usuario -> {
            Intent intent = new Intent(ListaUsuariosActivity.this, PerfilUsuario.class);
            intent.putExtra(CHAVE_USUARIO, usuario);
            startActivity(intent);
        });
    }

    private void configuraProgressBar() {
        progressBar = findViewById(R.id.progressBar);

    }
}