package com.example.usuarios.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.usuarios.OnItemClickListener;
import com.example.usuarios.R;
import com.example.usuarios.model.Usuario;

import java.util.List;

public class ListaUsuariosAdapter extends RecyclerView.Adapter<ListaUsuariosAdapter.ListaUsuariosViewHolder> {

    private final List<Usuario> todosUsuraios;
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ListaUsuariosAdapter(List<Usuario> todosUsuraios) {
        this.todosUsuraios = todosUsuraios;

    }

    @NonNull
    @Override
    public ListaUsuariosViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View viewCriada = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuario, parent, false);
        return new ListaUsuariosViewHolder(viewCriada);
    }

    @Override
    public void onBindViewHolder(@NonNull ListaUsuariosAdapter.ListaUsuariosViewHolder holder, int position) {
        Usuario usuario = todosUsuraios.get(position);
        holder.vincula(usuario);

    }

    @Override
    public int getItemCount() {
        return todosUsuraios.size();
    }

    class ListaUsuariosViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imagem;
        private final TextView nome;
        private Usuario usuario;
        public ListaUsuariosViewHolder(@NonNull  View itemView) {
            super(itemView);

            itemView.setOnClickListener(v -> onItemClickListener.onItemClick(usuario));

            nome = itemView.findViewById(R.id.item_usuario_nome);
            imagem = itemView.findViewById(R.id.item_usuario_imagem);
        }

        public void vincula(Usuario usuario) {
            this.usuario = usuario;
            nome.setText(usuario.getFirstname().concat(" ").concat(usuario.getLastname()));
            usuario.setImage("https://i.ytimg.com/vi/1Bjcs1Siy38/maxresdefault.jpg");
            String imagemURL = "https://i.ytimg.com/vi/1Bjcs1Siy38/maxresdefault.jpg";//usuario.getImage();
            Glide.with(imagem).load(imagemURL).centerCrop().into(imagem);
        }

    }
}
