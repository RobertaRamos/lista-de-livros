package com.example.usuarios;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.usuarios.model.Usuario;

import static com.example.usuarios.ListaUsuariosActivity.CHAVE_USUARIO;

public class PerfilUsuario extends AppCompatActivity {

    public static final String TITLE_APPBAR = "Perfil do usuario";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);

        Intent intentRecebida = getIntent();
        if(intentRecebida.hasExtra(CHAVE_USUARIO)){
            Usuario usuarioSelecionado = (Usuario) intentRecebida.getSerializableExtra(CHAVE_USUARIO);
            inicializaCampos(usuarioSelecionado);
            setTitle(TITLE_APPBAR);
        }
    }

    private void inicializaCampos(Usuario usuario) {
        mostraNome(usuario);
        mostraID(usuario);
        mostraEmail(usuario);
        mostraEnderecoMAC(usuario);
        mostraIP(usuario);
        mostraNomeUsuario(usuario);
        mostraUsuarioSenha(usuario);
        mostraWebsite(usuario);
        mostraImagem(usuario);
    }

    private void mostraImagem(Usuario usuario) {
        ImageView imagem = findViewById(R.id.perfil_usuario_imagem);
        Glide.with(imagem).load(usuario.getImage()).circleCrop().into(imagem);
    }

    private void mostraWebsite(Usuario usuario) {
        TextView website = findViewById(R.id.perfil_usuario_website);
        website.setText(usuario.getWebsite());
    }

    private void mostraUsuarioSenha(Usuario usuario) {
        TextView usuarioSenha = findViewById(R.id.perfil_usuario_senha);
        usuarioSenha.setText(usuario.getLastname());
    }

    private void mostraNomeUsuario(Usuario usuario) {
        TextView nomeUsuario = findViewById(R.id.perfil_usuario_nome_usuario);
        nomeUsuario.setText(usuario.getUsername());
    }

    private void mostraIP(Usuario usuario) {
        TextView ip = findViewById(R.id.perfil_usuario_ip);
        ip.setText(usuario.getIp());
    }

    private void mostraEnderecoMAC(Usuario usuario) {
        TextView enderecoMAC = findViewById(R.id.perfil_usuario_endereco_mac);
        enderecoMAC.setText(usuario.getMacAddress());
    }

    private void mostraEmail(Usuario usuario) {
        TextView email = findViewById(R.id.perfil_usuario_email);
        email.setText(usuario.getEmail());
    }

    private void mostraID(Usuario usuario) {
        TextView id = findViewById(R.id.perfil_usuario_id);
        id.setText(usuario.getUuid());
    }

    private void mostraNome(Usuario usuario) {
        TextView nome = findViewById(R.id.perfil_usuario_nome);
        nome.setText(usuario.getFirstname().concat(" ").concat(usuario.getLastname()) );
    }

}