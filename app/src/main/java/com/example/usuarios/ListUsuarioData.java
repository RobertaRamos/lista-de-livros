package com.example.usuarios;

import com.example.usuarios.model.Usuario;

import java.util.List;

public class ListUsuarioData {
    List<Usuario> listUsuarioData;
    String mensagemFalha;

    public ListUsuarioData(List<Usuario> listUsuarioData) {
        this.listUsuarioData = listUsuarioData;
    }

    public ListUsuarioData(String mensagemFalha) {
        this.mensagemFalha = mensagemFalha;
    }

    public List<Usuario> getListLivroData() {
        return listUsuarioData;
    }

    public void setListLivroData(List<Usuario> listUsuarioData) {
        this.listUsuarioData = listUsuarioData;
    }

    public String getMensagemFalha() {
        return mensagemFalha;
    }

    public void setMensagemFalha(String mensagemFalha) {
        this.mensagemFalha = mensagemFalha;
    }
}
