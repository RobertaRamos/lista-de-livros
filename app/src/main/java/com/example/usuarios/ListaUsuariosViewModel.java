package com.example.usuarios;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.usuarios.model.Usuario;
import com.example.usuarios.network.UsuarioCallback;
import com.example.usuarios.repository.UsuariosRepository;

import java.util.List;

public class ListaUsuariosViewModel extends ViewModel {
    private UsuariosRepository usuarioRepository = new UsuarioRepositoryImp();

    private MutableLiveData<ListUsuarioData> listUsuarioData;

    public LiveData<ListUsuarioData> getLivro() {
        if (listUsuarioData == null) {
            listUsuarioData = new MutableLiveData<>();
        }

        usuarioRepository.getusuarioAPI(new UsuarioCallback() {
            @Override
            public void sucesso(List<Usuario> usuarios) {
                listUsuarioData.postValue(new ListUsuarioData(usuarios));
            }

            @Override
            public void falha(String mensagemFalha) {
                listUsuarioData.postValue(new ListUsuarioData(mensagemFalha));
            }
        });
        return listUsuarioData;
    }
}
